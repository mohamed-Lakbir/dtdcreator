import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.LinkedList;
public class DTDValider {
    private Document document;
    private String dtdPart="";
    final private String dtdPartEnd = " ]>";
    private String allChilds="";
    private String newFileString="";
    public DTDValider (String path) throws IOException, SAXException, ParserConfigurationException {
        prepeareXML(path);
        createDTDPart();
        create_DTD_XML_File(dtdPart+newFileString,path);
    }
    public void prepeareXML(String path) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        File xmlFile = new File(path);
        document = builder.parse(xmlFile);
        document.getDocumentElement().normalize();
        Element root = document.getDocumentElement();
        Reader reader = new FileReader(path);
        newFileString="";
        for(int c; (c=reader.read())!=-1;){
            newFileString = newFileString+Character.toString(c);
        }
    }
    public void createDTDPart(){
        Node rootElement = document.getFirstChild();
        NodeList nlist = rootElement.getChildNodes();
        LinkedList<Node> childElements = new LinkedList <>();
        childElements.addLast(rootElement);
        brightSearch(childElements,new LinkedList <String>() );
        dtdPart = "<?xml version=\"1.0\"?>"+"\n"+"<!DOCTYPE "+rootElement.getNodeName()+" [\n"+allChilds+dtdPartEnd;
    }
    public void brightSearch(LinkedList<Node>node,LinkedList<String>visitedNode){
        Node root = node.getFirst();
        NodeList nlist = root.getChildNodes();
        node.removeFirst();
        if(visitedNode.contains(root.getNodeName())){
            if(node.size()!=0){
                brightSearch(node,visitedNode);
            }
            }else{
                visitedNode.add(root.getNodeName());
                allChilds=allChilds+"<!ELEMENT "+ root.getNodeName()+ " (";
                boolean hashtagText = true;
                for (int i = 0 ; i < nlist.getLength() ; i++) {
                if (nlist.getLength()==1){
                    allChilds= allChilds + "#PCDATA";
                    break;
                }
                Node child = nlist.item(i);
                // process the child node here
                String nodeName =  child.getNodeName();
                if(hashtagText == true){
                    hashtagText=false;
                    continue;
                }
                if(hashtagText==false){
                    hashtagText=true;
                }
                    node.addLast(child);
                if(i==1){
                    allChilds = allChilds+ nodeName;
                }else {
                    allChilds = allChilds + ","+nodeName;
                }
            }
                allChilds = allChilds+ ")>\n";

            if(node.size()!=0){
                brightSearch(node,visitedNode);
            }
        }
    }
    public void create_DTD_XML_File(String xmlText,String xmlName) throws IOException {
        File fileDelte = new File(xmlName);
        fileDelte.delete();
        File file = new File(xmlName);
        file.createNewFile();
        PrintWriter writer = new PrintWriter(xmlName);
        writer.print(xmlText);
        writer.close();
    }
    public static void main (String []args) throws ParserConfigurationException, IOException, SAXException {
        DTDValider dtdValider = new DTDValider(JOptionPane.showInputDialog("Path name, from the file"));
    }
}