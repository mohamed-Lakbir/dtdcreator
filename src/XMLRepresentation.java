public class XMLRepresentation {

    private String xmlCode;
    private String dtdPart;

    public XMLRepresentation ( String xmlCode, String dtdPart ) {
        this.xmlCode = xmlCode;
        this.dtdPart = dtdPart;
    }

    public String getXmlCode () {
        return xmlCode;
    }

    public void setXmlCode ( String xmlCode ) {
        this.xmlCode = xmlCode;
    }

    public String getDtdPart () {
        return dtdPart;
    }

    public void setDtdPart ( String dtdPart ) {
        this.dtdPart = dtdPart;
    }
}
